#!/usr/bin/env bash

zoom_version=$(wget --spider https://zoom.us/client/latest/zoom_amd64.deb 2>&1 | grep Location | sed -e 's/.*prod\/\(.*\)\/.*/\1/')
actual_url=$(wget -q -O - "https://gitlab.com/axil/zoom-debian-repo/-/jobs/artifacts/main/file/version.txt?job=build" | grep _blank | cut -d '"' -f 8)

wget -q $actual_url

repo_version=$(cat version.txt)
rm version.txt

echo "Repo version: $repo_version"
echo "Zoom version: $zoom_version"

if [[ "$zoom_version" == "$repo_version" ]]; then
    exit 1
fi
