# Unofficial Zoom Debian/Ubuntu repository

Zoom doesn't provide a repository to update the Debian package. One has to visit the Zoom downloads page manually and download the package.

This project contains a CI/CD pipeline that downloads the latest Zoom package and creates a repository, which is then hosted with GitLab Pages. The pipeline runs once a day at 3:00 am UTC.

## Add the repository

To update to the latest Zoom version:

1. Add the Zoom repository:

   ```shell
   echo "deb [trusted=yes] https://axil.gitlab.io/zoom-debian-repo/ /" | sudo tee /etc/apt/sources.list.d/zoom.list
   ```

1. Install or update Zoom:

   ```shell
   sudo apt update
   sudo apt install zoom
   ```

If you get any errors about missing certificates, try installing the `ca-certificates` package.
